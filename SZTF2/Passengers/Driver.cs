﻿using SZTF2.Needs;
using SZTF2.Services;

namespace SZTF2.Passengers
{
    class Driver : Passenger, ISwitchNeed
    {
        public Driver(string Name, int needTimerInterval) : base(Name, needTimerInterval)
        {
        }

        public void Switched() => NeedTimerReset();
        protected override bool CanSkipCurrentStop(Stop stop, int passengerCount) => !IsNextStopSuitable(stop, typeof(ISwitchService)) ? false : base.CanSkipCurrentStop(stop, passengerCount);
        public override void OnStopReached(object sender, OnStopReachedEventArgs args)
        {
            var stop = sender as Stop;
            if (!(stop is ISwitchService service)) return;
            if (CanSkipCurrentStop(stop,args.PassengerCount)) return;
            service.SwitchDriver(this);
        }
    }
}
