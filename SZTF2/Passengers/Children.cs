﻿using SZTF2.Needs;
using SZTF2.Services;

namespace SZTF2.Passengers
{
    class Children : Passenger, IFoodNeed
    {
        public Children(string Name, int needTimerInterval) : base(Name, needTimerInterval)
        {
        }

        public void AteFood() => NeedTimerReset();

        protected override bool CanSkipCurrentStop(Stop stop, int passengerCount) => !IsNextStopSuitable(stop, typeof(IFoodService)) ? false : base.CanSkipCurrentStop(stop, passengerCount);
        public override void OnStopReached(object sender, OnStopReachedEventArgs args)
        {
            var stop = sender as Stop;
            if (!(stop is IFoodService service)) return;
            if (CanSkipCurrentStop(stop,args.PassengerCount)) return;
            service.EatFood(this);
        }
    }
}
