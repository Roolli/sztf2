﻿using SZTF2.Needs;
using SZTF2.Services;

namespace SZTF2.Passengers
{
    class BeerDrinkers : Passenger, IToilettNeed
    {

        public BeerDrinkers(string name, int needTimerInterval) : base(name, needTimerInterval)
        {

        }

        public void UsedToilett() => NeedTimerReset();
        protected override bool CanSkipCurrentStop(Stop stop, int passengerCount) => !IsNextStopSuitable(stop, typeof(IToilettService)) ? false : base.CanSkipCurrentStop(stop, passengerCount);

        public override void OnStopReached(object sender, OnStopReachedEventArgs args)
        {
            var stop = sender as Stop;
            if (!(stop is IToilettService service)) return;
            if (CanSkipCurrentStop(stop, args.PassengerCount)) return;
            service.UseToilett(this);
        }
    }
}
