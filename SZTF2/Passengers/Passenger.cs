﻿using System;

namespace SZTF2
{
    abstract class Passenger
    {

        /// <summary>
        /// In Minutes
        /// </summary>
        public readonly int NeedTimerInterval;
        public string Name { get; private set; }

        public Passenger(string Name, int needTimerInterval)
        {
            this.Name = Name;
            NeedTimerInterval = needTimerInterval;
            NeedTimer = NeedTimerInterval;
        }

        public void OnTimePassed(object sender, TimePassedEventArgs e) => TimerAdjust(e.MinutesPassed);
        /// <summary>
        /// In Minutes
        /// </summary>
        public int NeedTimer { get; private set; }
        protected void NeedTimerReset() => NeedTimer = NeedTimerInterval;

        private void TimerAdjust(int value)
        {
            if (NeedTimer - value < 0)
            {
                throw new PassengerNeedNotFulfilledException($"The following passenger had their timer reach 0: {Name} ({GetType().Name})");
            }
            NeedTimer -= value;
        }
        public abstract void OnStopReached(object sender, OnStopReachedEventArgs args);

        protected virtual bool CanSkipCurrentStop(Stop stop, int passengerCount)
        {
          
            double relativeDistance = stop.Next.DistanceFromStart - stop.DistanceFromStart;
            double time = (int)Math.Ceiling((relativeDistance / Bus.CurrentSpeed) * 60) + passengerCount;
            return NeedTimer - time > 0;
        }
        protected bool IsNextStopSuitable(Stop stop, params Type[] interfaces)
        {
            for (var i = 0; i < interfaces.Length; i++)
            {
                if (!interfaces[i].IsInstanceOfType(stop) || !interfaces[i].IsInstanceOfType(stop.Next))
                {
                    return false;
                }
            }
            return true;
        }

    }
    class PassengerNeedNotFulfilledException : Exception
    {
        public PassengerNeedNotFulfilledException(string msg) : base(msg, null)
        {

        }
    }

}
