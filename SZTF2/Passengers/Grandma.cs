﻿using SZTF2.Needs;
using SZTF2.Services;

namespace SZTF2.Passengers
{
    class Grandma : Passenger, IRestNeed
    {
        public Grandma(string Name, int needTimerInterval) : base(Name, needTimerInterval)
        {
        }

        public void Rested() => NeedTimerReset();
        protected override bool CanSkipCurrentStop(Stop stop, int passengerCount) => !IsNextStopSuitable(stop, typeof(IRestService)) ? false : base.CanSkipCurrentStop(stop, passengerCount);
        public override void OnStopReached(object sender, OnStopReachedEventArgs eventArgs)
        {
            var stop = sender as Stop;
            if (!(stop is IRestService service)) return;
            if (CanSkipCurrentStop(stop, eventArgs.PassengerCount)) return;
            service.Rest(this);
        }
    }
}
