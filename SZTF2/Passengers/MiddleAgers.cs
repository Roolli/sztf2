﻿using SZTF2.Needs;
using SZTF2.Services;

namespace SZTF2.Passengers
{
    class MiddleAgers : Passenger, IToilettNeed, ICoffeeNeed
    {
        public MiddleAgers(string name, int needTimerInterval) : base(name, needTimerInterval)
        {
        }

        public void DrankCoffee() => NeedTimerReset();

        public void UsedToilett() => NeedTimerReset();
        protected override bool CanSkipCurrentStop(Stop stop, int passengerCount) =>
         !IsNextStopSuitable(stop, typeof(IToilettService), typeof(ICoffeeService))? false : base.CanSkipCurrentStop(stop, passengerCount);

        public override void OnStopReached(object sender, OnStopReachedEventArgs args)
        {
            var stop = sender as Stop;
            if (!(stop is ICoffeeService) || !(stop is IToilettService)) return;
            if (CanSkipCurrentStop(stop, args.PassengerCount)) return;
            (stop as ICoffeeService).DrinkCoffee(this);
            (stop as IToilettService).UseToilett(this);



        }
    }
}
