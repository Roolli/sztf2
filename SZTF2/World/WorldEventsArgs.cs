﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SZTF2
{

    public class TimePassedEventArgs
    {
        public int MinutesPassed { get; set; }
    }
    public class DistanceTravelledEventArgs
    {
        public double DistancePerMinute { get; set; }
        public double TotalDistance { get; set; }
        public int TimeInterval { get; set; }
        public int TimeElapsed { get; set; }
        public int PassengerCount { get; set; }
    }
}
