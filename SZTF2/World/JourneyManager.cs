﻿using LinkedList;
using System;
using System.Linq;
using SZTF2.Data;
using SZTF2.Services;
using SZTF2.UI;

namespace SZTF2.World
{
    public class JourneyManager
    {
        private readonly double TotalDistance = 0;
        private double TravelledDistance = 0.0;
        private int CurrentTime = 0;
        private MyLinkedList<Stop> stops;
        private readonly MyLinkedList<Passenger> passengers;
        public event EventHandler<TimePassedEventArgs> TimePassed;
        public event EventHandler<DistanceTravelledEventArgs> DistanceTravelled;
        public JourneyManager(string filePath = "")
        {
            passengers = DataHandler.ParsePassengers(FileManager.ReadFile("./Data/Passengers.txt"));
            stops = filePath == string.Empty ? DataHandler.ParseStops() : DataHandler.ParseStops(FileManager.ReadFile(filePath));
            SetupEventHandlers();
            TotalDistance = stops[stops.Count - 1].DistanceFromStart;
        }

        private void SetupEventHandlers()
        {
            DistanceTravelled += Bus.OnDistanceTravelled;
            foreach (ListItem<Passenger> item in passengers)
            {
                TimePassed += item.Value.OnTimePassed;
            }
            foreach (ListItem<Stop> stop in stops)
            {
                stop.Value.TimePassed = TimePassed;
                DistanceTravelled += stop.Value.OnDistanceTravelled;
                foreach (ListItem<Passenger> passenger in passengers)
                {
                    stop.Value.StopReached += passenger.Value.OnStopReached;
                }
            }
        }

        public void BeginSimulation()
        {
            if (!IsJourneyPossible()) throw new JourneyNotPossibleException();
            while (TravelledDistance < TotalDistance)
            {
                ConsoleManager.OutputPassengerList(passengers);
                ConsoleManager.OutputJourneyDetails(TravelledDistance, CurrentTime, stops.Count);
                string key = ConsoleManager.ProcessInput();
                AdvanceTime(key);
            }
            GetActualStops();
            ConsoleManager.AnnounceTimeTable(stops.Select(s => new Tuple<int, int>(s.Index, s.ArrivedAt)).OrderBy(t => t.Item1).ToArray());
        }

        private void GetActualStops() => stops = stops.Where(s => s.GotOffCount > 0).ToMyLinkedList();

        private bool IsJourneyPossible()
        {
            int currDistance = 0;
            double maxFuelDistance = (Constants.BUS_FUEL_TANK_SIZE + 0.0) / Constants.BUS_FUEL_CONSUMPTION_PER_HUNDRED * 100;
            if (TotalDistance > maxFuelDistance && stops.Count(s => s is IGasService) == 0)
            {
                return false;
            }
            foreach (ListItem<Stop> item in stops.Where(s => s is IGasService).ToMyLinkedList())
            {
                if (item.Value.DistanceFromStart - currDistance > maxFuelDistance)
                {
                    return false;
                }
                currDistance += item.Value.DistanceFromStart - currDistance;
            }
            return currDistance + maxFuelDistance > TotalDistance ? true : false;
        }

        private void AdvanceTime(string key)
        {
            int minutes = 0;
            switch (key.ToUpper())
            {
                case "Q":
                    minutes = 30;
                    break;
                case "W":
                    minutes = 20;
                    break;
                case "E":
                    minutes = 10;
                    break;
                case "R":
                    minutes = 5;
                    break;
                case "T":
                    minutes = 1;
                    break;
            }

            for (int i = 0; i < minutes; i++)
            {
                CurrentTime++;
                OnTimePassed(1);
                TravelledDistance += Bus.CurrentSpeed / 60.0;
                DistanceTravelled?.Invoke(this, new DistanceTravelledEventArgs() { TotalDistance = TravelledDistance, TimeElapsed = CurrentTime, PassengerCount = passengers.Count });
            }


        }
        public void OnTimePassed(int v) => TimePassed?.Invoke(this, new TimePassedEventArgs() { MinutesPassed = v });
    }

    internal class JourneyNotPossibleException : Exception
    {
        public JourneyNotPossibleException() : base("Journey Not possible because there is not enough fuel to last between 2 stations")
        {
        }
    }

}
