﻿using System;

namespace SZTF2
{
    class Bus
    {
        public static double DistanceTravelled { get; private set; }
        static readonly int FuelConsumption = Constants.BUS_FUEL_CONSUMPTION_PER_HUNDRED;
        public static int FuelTank { get; private set; } = Constants.BUS_FUEL_TANK_SIZE;
        public static int CurrentSpeed { get; private set; } = 90;

        public static void OnDistanceTravelled(object sender, DistanceTravelledEventArgs args)
        {
            var distanceDelta = (args.DistancePerMinute * args.TimeInterval);
            FuelTank -= (int)Math.Floor(FuelConsumption / 100.0 * args.TotalDistance - distanceDelta);
            DistanceTravelled += (args.DistancePerMinute * args.TimeInterval);

        }
        public static void SetSpeed(int NewValue) => CurrentSpeed = NewValue;

        public static void Refueled() => FuelTank = Constants.BUS_FUEL_TANK_SIZE;
    }
}
