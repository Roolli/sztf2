﻿using System;
using System.IO;
using System.Linq;

namespace SZTF2
{
    class Constants
    {
        public static int DRIVER_SWITCH_INTERVAL { get; private set; } = 240; //Minutes
        public static int GRANDMA_REST_INTERVAL { get; private set; } = 180;//Minutes
        public static int ALCOHOLIC_PEE_INTERVAL { get; private set; } = 120;//Minutes
        public static int CHILDREN_EAT_INTERVAL { get; private set; } = 300;//Minutes
        public static int MIDDLEAGLE_NEEDS_INTERVAL { get; private set; } = 240;//Minutes
        public static int BUS_FUEL_CONSUMPTION_PER_HUNDRED { get; private set; } = 30; //L/100km
        public static int BUS_FUEL_TANK_SIZE { get; private set; } = 150; //Liter

        public static void ReadValues(string filePath)
        {
            if (File.Exists(filePath))
            {
                var contents = FileManager.ReadFile(filePath).Select(s => int.Parse(s)).ToArray();
                DRIVER_SWITCH_INTERVAL = contents[0];
                GRANDMA_REST_INTERVAL = contents[1];
                ALCOHOLIC_PEE_INTERVAL = contents[2];
                CHILDREN_EAT_INTERVAL = contents[3];
                MIDDLEAGLE_NEEDS_INTERVAL = contents[4];
                BUS_FUEL_CONSUMPTION_PER_HUNDRED = contents[5];
                BUS_FUEL_TANK_SIZE = contents[6];
            }
            else
            {
                Console.WriteLine("No constant overload file was found: using the default values");
            }
        }
    }
}