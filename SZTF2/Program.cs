﻿using System;
using SZTF2.World;
namespace SZTF2
{
    internal class Program
    {
        private static void Main(params string[] args)
        {
            JourneyManager jm;
            if (args.Length == 2 && args[0].ToLower() == "-f")
            {
                string filePath = args[1];
                jm = new JourneyManager(filePath);
            }
            else
            {
                jm = new JourneyManager();
            }
            Constants.ReadValues("./Data/constants.txt");
            try
            {
                jm.BeginSimulation();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            _ = Console.ReadKey();
        }
    }
}
