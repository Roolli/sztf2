﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using LinkedList;

namespace SZTF2
{
    class FileManager
    {
        public static string[] ReadFile(string fileName)
        {
            var lines = new MyLinkedList<string>();
            if (File.Exists(fileName))
            {
                using (var sr = new StreamReader(fileName))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        if (line.Contains('#'))
                            line = line.Substring(0, line.IndexOf('#'));

                        if (line != "")
                            lines.Insert(line);

                    }
                }
                return lines.ToArray();
            }
            else
            {
                throw new FileNotFoundException($"The following file was not found: {fileName}");
            }
        }
    }
}
