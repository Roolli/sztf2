﻿using LinkedList;
using System;
using System.Linq;
using System.Reflection;
using SZTF2.Passengers;

namespace SZTF2.Data
{
    static class DataHandler
    {
        public static MyLinkedList<Passenger> ParsePassengers(string[] data)
        {
            var passengers = new MyLinkedList<Passenger>();
            foreach (string item in data)
            {
                //#Name;PassengerType(1- driver,2-grandma,3-beerdrinkers,4-children,5-middleagers)

                string[] result = item.Split(';');
                int type = int.Parse(result[1].Trim());
                switch (type)
                {
                    case 1:
                        passengers.Insert(new Driver(result[0].Trim(), Constants.DRIVER_SWITCH_INTERVAL));
                        break;
                    case 2:
                        passengers.Insert(new Grandma(result[0].Trim(), Constants.GRANDMA_REST_INTERVAL));
                        break;
                    case 3:
                        passengers.Insert(new BeerDrinkers(result[0].Trim(), Constants.ALCOHOLIC_PEE_INTERVAL));
                        break;
                    case 4:
                        passengers.Insert(new Children(result[0].Trim(), Constants.CHILDREN_EAT_INTERVAL));
                        break;
                    case 5:
                        passengers.Insert(new MiddleAgers(result[0], Constants.MIDDLEAGLE_NEEDS_INTERVAL));
                        break;
                }
            }
            return passengers;
        }
        public static MyLinkedList<Stop> ParseStops(string[] data)
        {
            var stops = new MyLinkedList<Stop>();
            foreach (string item in data)
            {
                //distance from start;type (1 gas,2 toilett,3 mcdonalds,4 city)
                int[] result = item.Split(';').Select(c => int.Parse(c.Trim())).ToArray();
                Stop stop = null;
                switch (result[1])
                {
                    case 1:
                        stop = new GasStation(result[0]);
                        break;
                    case 2:
                        stop = new ToilettPlace(result[0]);
                        break;
                    case 3:
                        stop = new McDonalds(result[0]);
                        break;
                    case 4:
                        stop = new City(result[0]);
                        break;
                }
                stops.Insert(stop);
            }
            stops = SortStops(stops);
            for (int i = 1; i < stops.Count - 1; i++)
            {
                stops[i - 1].Next = stops[i];
            }
            return stops;
        }
        public static MyLinkedList<Stop> ParseStops()
        {
            var stops = new MyLinkedList<Stop>();
            int totalDistance = 2500;
            int toiletInterval = 2;
            int mcDonaldsInterval = 20;
            int gasStationInterval = 10;
            int cityInterval = 50;

            GenerateStop(totalDistance, stops, toiletInterval, typeof(ToilettPlace));
            GenerateStop(totalDistance, stops, mcDonaldsInterval, typeof(McDonalds));
            GenerateStop(totalDistance, stops, gasStationInterval, typeof(GasStation));
            GenerateStop(totalDistance, stops, cityInterval, typeof(City));
            stops = SortStops(stops);
            for (int i = 1; i < stops.Count - 1; i++)
            {
                stops[i - 1].Next = stops[i];
            }
            return stops;
        }

        private static MyLinkedList<Stop> SortStops(MyLinkedList<Stop> stops)
        {
            Stop[] temp = stops.ToArray();
            Array.Sort(temp);
            return temp.ToMyLinkedList();
        }

        private static void GenerateStop(int totalDistance, MyLinkedList<Stop> stops, int stopInterval, Type t)
        {
            ConstructorInfo ctor = t.GetConstructor(new[] { typeof(int) });
            for (int i = 0; i < totalDistance / stopInterval; i++)
            {
                stops.Insert((Stop)ctor.Invoke(new object[] { i * stopInterval }));
            }
        }
    }
}
