﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SZTF2.Needs;

namespace SZTF2.Services
{
    interface ISwitchService
    {
        void SwitchDriver(ISwitchNeed driver);
    }
}
