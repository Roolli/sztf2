﻿using LinkedList;
using System;
using System.Text;
using System.Text.RegularExpressions;
using SZTF2.Passengers;

namespace SZTF2.UI
{
#pragma warning disable IDE0058 // Expression value is never used
    class ConsoleManager
    {
        public static void OutputPassengerList(MyLinkedList<Passenger> passengerList)
        {
            Console.Clear();
            var sb = new StringBuilder();
            foreach (ListItem<Passenger> item in passengerList)
            {

                sb.Append($"{item.Value.Name}-");

                switch (item.Value)
                {
                    case BeerDrinkers b:
                        sb.Append("sörözgető fiatalok-");
                        break;
                    case Grandma g:
                        sb.Append("nagymamák-");
                        break;
                    case Children c:
                        sb.Append("gyerekek-");
                        break;
                    case MiddleAgers m:
                        sb.Append("középkorúak-");
                        break;
                    case Driver d:
                        sb.Append("sofőrök-");
                        break;
                    default:
                        sb.Append("Unknown-");
                        break;
                }
                sb.AppendLine(item.Value.NeedTimer.ToString());
            }
            Console.WriteLine(sb.ToString());
        }
#pragma warning restore IDE0058 // Expression value is never used
        public static void OutputJourneyDetails(double DistanceTravelled, int TimePassed, int stopCount) => Console.WriteLine($"distance:{DistanceTravelled:0.##} km time passed: {TimePassed} total stops:{stopCount}");
        public static string ProcessInput()
        {
            char key;
            do
            {
                key = Console.ReadKey(true).KeyChar;
            }
            while (!Regex.IsMatch(key.ToString(), @"^[qwert]?$", RegexOptions.IgnoreCase));
            return key.ToString();
        }
        public static void AnnounceTimeTable(Tuple<int, int>[] times)
        {
            Console.Clear();
            var i = 0;
            for (; i < times.Length - 1; i++)
            {
                Console.WriteLine($"The bus will reach stop Id {times[i].Item1} after {times[i].Item2} minutes from start!");
            }
            Console.WriteLine($"The bus will reach the end of the journey after {times[i].Item2} minutes from start!");
            Console.WriteLine("press 'ENTER' to continue");
            Console.WriteLine(times.Length);
            ConsoleKey key;
            do
            {
                key = Console.ReadKey(true).Key;
            }
            while (key != ConsoleKey.Enter);
        }
    }
}
