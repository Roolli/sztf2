﻿using System;
using SZTF2.World;

namespace SZTF2
{
    class Stop : IComparable
    {
        static int StopIndex = 0;
        public readonly int Index;
        public int GotOffCount { get; protected set; } = 0;
        public event EventHandler<OnStopReachedEventArgs> StopReached;
        public EventHandler<TimePassedEventArgs> TimePassed { get; set; }
        public int DistanceFromStart { get; private set; }
        public Stop Next { get; set; }

        public int ArrivedAt { get; protected set; }

        public Stop(int distanceFromStart)
        {
            DistanceFromStart = distanceFromStart;
            Index = StopIndex++;
        }

        public virtual void OnDistanceTravelled(object sender, DistanceTravelledEventArgs e)
        {
            //Before the event was fired
            if (e.TotalDistance >= DistanceFromStart)
            {
                OnStopReached(e.PassengerCount);
                ArrivedAt = e.TimeElapsed;
                OnTimePassed();
                (sender as JourneyManager).DistanceTravelled -= OnDistanceTravelled;
                return;
            }

        }
        protected void OnTimePassed()
        {
            if (GotOffCount > 0)
            {
                TimePassed?.Invoke(this, new TimePassedEventArgs() { MinutesPassed = GotOffCount });
            }
        }
        protected void OnStopReached(int passengerCount) => StopReached?.Invoke(this, new OnStopReachedEventArgs() { PassengerCount = passengerCount });

        public int CompareTo(object obj)
        {
            var other = obj as Stop;
            return other.DistanceFromStart > DistanceFromStart ? -1 : other.DistanceFromStart < DistanceFromStart ? 1 : 0;

        }
    }
}
