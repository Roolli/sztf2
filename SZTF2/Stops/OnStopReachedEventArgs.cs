﻿namespace SZTF2
{
    class OnStopReachedEventArgs
    {
        public int PassengerCount { get; set; }
    }
}
