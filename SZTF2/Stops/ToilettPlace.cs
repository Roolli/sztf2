﻿using SZTF2.Needs;
using SZTF2.Services;

namespace SZTF2
{
    class ToilettPlace : Stop, IToilettService, IRestService, ISwitchService
    {
        public ToilettPlace(int distanceFromStart) : base(distanceFromStart)
        {
        }

        public void Rest(IRestNeed person)
        {
            GotOffCount++;
            person.Rested();
        }

        public void SwitchDriver(ISwitchNeed driver)
        {
            GotOffCount++;
            driver.Switched();
        }

        public void UseToilett(IToilettNeed person)
        {
            GotOffCount++;
            person.UsedToilett();
        }
    }
}
