﻿using SZTF2.Needs;
using SZTF2.Services;

namespace SZTF2
{
    class GasStation : Stop, IRestService, ICoffeeService, IGasService, IToilettService
    {
        public GasStation(int distanceFromStart) : base(distanceFromStart)
        {
        }

        public void DrinkCoffee(ICoffeeNeed person)
        {
            GotOffCount++;
            person.DrankCoffee();
        }

        public void Refuel() => Bus.Refueled();

        public void Rest(IRestNeed person)
        {
            GotOffCount++;
            person.Rested();
        }

        public void UseToilett(IToilettNeed person)
        {
            GotOffCount++;
            person.UsedToilett();
        }
    }
}
