﻿using SZTF2.Needs;
using SZTF2.Services;

namespace SZTF2
{
    class McDonalds : Stop, IFoodService, ICoffeeService, IToilettService, IRestService, ISwitchService
    {
        public McDonalds(int distanceFromStart)
        : base(distanceFromStart)
        {
        }

        public void DrinkCoffee(ICoffeeNeed person)
        {
            GotOffCount++;
            person.DrankCoffee();
        }

        public void EatFood(IFoodNeed person)
        {
            GotOffCount++;
            person.AteFood();
        }

        public void Rest(IRestNeed person)
        {
            GotOffCount++;
            person.Rested();
        }

        public void SwitchDriver(ISwitchNeed driver)
        {
            GotOffCount++;
            driver.Switched();
        }

        public void UseToilett(IToilettNeed person)
        {
            GotOffCount++;
            person.UsedToilett();
        }
    }
}
