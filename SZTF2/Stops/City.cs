﻿using System;
using SZTF2.Needs;
using SZTF2.Services;
using SZTF2.World;

namespace SZTF2
{
    class City : Stop, IRestService, IFoodService, IToilettService, ISwitchService, ICoffeeService
    {
        // to ensure we don't trigger the stop event again, but still check if we are in range of the city
        bool reachedCity = false;
        public City(int distanceFromStart) : base(distanceFromStart)
        {

        }

        public override void OnDistanceTravelled(object sender, DistanceTravelledEventArgs e)
        {

            if (e.TotalDistance >= DistanceFromStart && !reachedCity)
            {
                Bus.SetSpeed(30);
                OnStopReached(e.PassengerCount);
                OnTimePassed();
                reachedCity = true;
                ArrivedAt = e.TimeElapsed;
            }
            if (e.TotalDistance >= DistanceFromStart + Scale)
            {
                Bus.SetSpeed(90);
                (sender as JourneyManager).DistanceTravelled -= OnDistanceTravelled;
            }
        }

        public int Scale { get; private set; } = new Random().Next(4, 11);

        public void DrinkCoffee(ICoffeeNeed person)
        {
            GotOffCount++;
            person.DrankCoffee();
        }

        public void EatFood(IFoodNeed person)
        {
            GotOffCount++;
            person.AteFood();
        }

        public void Rest(IRestNeed person)
        {
            GotOffCount++;
            person.Rested();
        }

        public void SwitchDriver(ISwitchNeed driver)
        {
            GotOffCount++;
            driver.Switched();
        }

        public void UseToilett(IToilettNeed person)
        {
            GotOffCount++;
            person.UsedToilett();
        }
    }
}
