﻿using System.Collections;
using System.Collections.Generic;

namespace LinkedList
{
    public class ListItemEnum<T> : IEnumerator<T>
    {
        ListItem<T> _List;
        public ListItem<T> Current { get; private set; } = null;
        public ListItemEnum(ListItem<T> List) => _List = List;
        object IEnumerator.Current => Current;

        T IEnumerator<T>.Current => Current.Value;

        public bool MoveNext()
        {
            if (_List == null)
            {
                return false;
            }
            Current = _List;

            _List = _List.Next;
            return true;
        }

        public void Reset() => Current = null;

        public void Dispose() => _List = null;
    }
}
