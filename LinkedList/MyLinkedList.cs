﻿using System;
using System.Collections;

namespace LinkedList
{
    public class MyLinkedList<T> : System.Collections.Generic.IEnumerable<T>
    {
        /// <summary>
        /// First element of the list
        /// </summary>
        private ListItem<T> Head;
        /// <summary>
        /// Number of elements in the List
        /// </summary>
        public int Count { get; private set; }
        /// <summary>
        /// Inserts an element to the end of the List
        /// </summary>
        /// <param name="element"></param>
        public void Insert(T element)
        {
            if (IsEmpty())
            {
                Head = new ListItem<T>(element);
            }
            else
            {
                ListItem<T> actual = Head;
                while (actual.Next != null)
                {
                    actual = actual.Next;
                }
                actual.Next = new ListItem<T>(element);
            }
            Count++;
        }
        /// <summary>
        /// Finds the ListItem with the supplied value
        /// </summary>
        /// <param name="value"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        /// <returns></returns>
        public ListItem<T> Find(T value)
        {
            ListItem<T> actual = Head;
            if (IsEmpty()) throw new IndexOutOfRangeException("The List is Empty");
            while (!actual.Value.Equals(value) && actual.Next != null)
            {
                actual = actual.Next;
            }
            return actual.Value.Equals(value) ? actual : null;
        }
        /// <summary>
        /// Deletes the first occurrence of the value
        /// </summary>
        /// <param name="deleteable"></param>
        /// <returns></returns>
        public bool RemoveFirst(T deleteable)
        {
            ListItem<T> result = Find(deleteable);
            if (result != null)
            {
                ListItem<T> actual = Head;
                ListItem<T> previous = actual;
                while (actual != result)
                {
                    previous = actual;
                    actual = actual.Next;
                }
                if (actual.Next != null)
                {
                    ListItem<T> next = actual.Next;
                    actual.Value = next.Value;
                    actual.Next = next.Next;
                }
                else if (actual == result)
                {
                    previous.Next = null;
                }
                Count--;
                return true;
            }
            return false;
        }
        //Needs checking
        /// <summary>
        /// Removes the last occurrence of the value
        /// </summary>
        /// <param name="deleteable"></param>
        /// <returns>True if the removal was successful, false otherwise</returns>
        public bool RemoveLast(T deleteable)
        {
            ListItem<T> result = Find(deleteable);
            if (result != null)
            {
                ListItem<T> actual = Head;
                //ListItem<T> previous = actual;
                while (actual.Next != null)
                {
                    // previous = actual;
                    actual = actual.Next;
                    if (actual.Value.Equals(actual.Value))
                    {
                        result = actual;
                    }
                }
                if (result.Next != null)
                {
                    ListItem<T> next = result.Next;
                    actual.Value = next.Value;
                    actual.Next = next.Next;
                }
                else
                {
                    result = null;
                }
                Count--;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Removes the item for the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        /// <returns>True if the removal was successfull,false otherwise</returns>
        public bool RemoveAt(int index)
        {
            if (IsEmpty()) throw new IndexOutOfRangeException("The list is Empty");
            if (index >= Count) throw new IndexOutOfRangeException("The index exceeded the limit of the list");
            ListItem<T> actual = Head;
            ListItem<T> previous = actual;
            int currIndex = 0;
            while (actual.Next != null && currIndex != index)
            {
                previous = actual;
                actual = actual.Next;
                currIndex++;
            }

            if (actual.Next != null)
            {
                ListItem<T> next = actual.Next;
                actual.Value = next.Value;
                actual.Next = next.Next;
                Count--;
                return true;
            }
            else if (actual.Next == null)
            {
                previous.Next = null;
                Count--;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Converts the LinkedList to a List<T>
        /// </summary>
        /// 
        /// <returns></returns>
        public System.Collections.Generic.List<T> ToList()
        {
            var values = new System.Collections.Generic.List<T>();
            if (IsEmpty()) throw new IndexOutOfRangeException("The List is empty!");
            ListItem<T> current = Head;
            while (current.Next != null)
            {
                values.Add(current.Value);
                current = current.Next;
            }
            if (current.Value != null)
            {
                values.Add(current.Value);
            }
            return values;
        }
        /// <summary>
        /// Allows array-like indexing for the List
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public T this[int i]
        {
            get => GetElementAt(i);

            set => SetElementAt(value, i);
        }
        /// <summary>
        /// Sets the element value at the given index
        /// </summary>
        /// <param name="value"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        /// <param name="i"></param>
        private void SetElementAt(T value, int i)
        {
            int currIndex = 0;
            if (Head == null)
            {
                Head = new ListItem<T>(value);
                Count++;
                return;
            }
            if (i == 0)
            {
                Head.Value = value;
                return;
            }
            ListItem<T> actual = Head;
            while (actual.Next != null && currIndex < i)
            {
                actual = actual.Next;
                currIndex++;
            }
            if (currIndex == i)
            {
                var item = new ListItem<T>(value);
                actual.Next = item;
            }
            else if (currIndex > i)
            {
                throw new IndexOutOfRangeException();
            }
            else
            {
                actual.Value = value;
            }
        }
        /// <summary>
        /// Empties the List
        /// </summary>
        public void Clear()
        {
            while (Head != null)
            {
                Head.Value = default;
                Head = Head.Next;
            }
            Count = 0;
        }
        /// <summary>
        /// Inserts to the beginning of the List
        /// </summary>
        /// <param name="value"></param>
        public void InsertToBeginning(T value)
        {
            if (Head == null)
            {
                Head = new ListItem<T>(value);
            }
            else
            {
                ListItem<T> actual = Head;
                var newHead = new ListItem<T>(value) { Next = actual };
                Head = newHead;
            }
            Count++;
        }

        /// <summary>
        /// Gets the element at the given index
        /// </summary>
        /// <param name="index"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        /// <returns></returns>
        private T GetElementAt(int index)
        {
            if (IsEmpty() || index > Count) throw new IndexOutOfRangeException();
            int currIndex = 0;
            ListItem<T> actual = Head;
            while (actual.Next != null && currIndex != index)
            {
                actual = actual.Next;
                currIndex++;
            }
            return currIndex == index ? actual.Value : (default);

        }
        private T[] ToArray()
        {
            var values = new T[Count];
            for (int i = 0; i < Count; i++)
            {
                values[i] = GetElementAt(i);
            }
            return values;
        }
        /// <summary>
        /// Checks if the List is empty
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty() => Head == null ? true : false;
        /// <summary>
        /// Finds all the indicies of the values that match in the list with the supplied element 
        /// </summary>
        /// <param name="element"></param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        /// <returns> array of indices that match the supplied element</returns>
        public int[] FindAll(T element)
        {
            if (IsEmpty()) throw new IndexOutOfRangeException("The list is empty");
            var indices = new MyLinkedList<int>();
            ListItem<T> actual = Head;
            int currIndex = 0;
            while (actual.Next != null)
            {
                if (actual.Value.Equals(element))
                {
                    indices.Insert(currIndex);
                }
                currIndex++;
                actual = actual.Next;
            }
            return indices.ToArray();
        }

        public ListItemEnum<T> GetEnumerator() => new ListItemEnum<T>(Head);
        System.Collections.Generic.IEnumerator<T> System.Collections.Generic.IEnumerable<T>.GetEnumerator() => GetEnumerator();


        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public MyLinkedList<T> DeepCopy()
        {
            var output = new MyLinkedList<T>();
            foreach (ListItem<T> item in this)
            {
                output.Insert(item.Value);
            }
            return output;
        }

    }
}
