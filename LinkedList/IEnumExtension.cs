﻿using System.Collections.Generic;


namespace LinkedList
{
    public static class IEnumExtension
    {
        /// <summary>
        /// Converts the given IEnumerable to a LinkedList 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        /// <returns></returns>
        public static MyLinkedList<T> ToMyLinkedList<T>(this IEnumerable<T> result)
        {
            var list = new MyLinkedList<T>();
            foreach (var item in result)
            {
                list.Insert(item);
            }
            return list;
        }
    }
}
