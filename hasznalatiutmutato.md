# **SZTF2 Féléves feladat: ( Ott vagyunk már?) KA/9**

## Készítette: **Mikhel Roland (W68OXV)**

## Program futására vonatkozó információk

- Ha parancssorból (MINGW ből nem) inidítjuk a programot akkor -f flaggel lehet megadni saját megállókat tartalmazó fájl,
  pl -f pelda.txt (relatív útvonal az exéhez képest) ellenkező esetben 1 sajátot csinál a program előre definiált értékekből (lásd: DataHandler class)
- A feladatban szereplő konstants értékek felülírhatók egy constants.txt nevű fájlal a Data mappában,
  a [FájlFormátumok](#FájlFormátumok) fájl formátum a fájl formátumok részben található.
- A fájlokban a '#' veszi fel a komment szerepét minden ami a '#' után van nem kerül bele a programba

## Fájlformátumok

    Stops.txt: [távolság a kiindulási ponttól];[megállótípus(szám)]
        Megállótípus lehet:
            - 1:Benzinkút
            - 2:pihenőhely (WC)
            - 3:mcDonalds
            - 4:Város
     Passengers.txt: [Név];[UtasTípus(szám)]
        Utastipus lehet:
             1: Sofőr
             2: Nagymama
             3: Sörözgető fiatalok
             4: Gyerekek
             5: Középkorúak
     Constants.txt:
             Söfőr Csere idő (Perc)
             Nagymama derékmozgatás idő (Perc)
             Sörözgető fiatalok WC idő (Perc)
             Gyerekek éves idő (Perc)
             Középkorú kávé és WC idő (Perc)
             Busz fogyasztás (Liter/100km)
             Busz Tank méret (Liter)
